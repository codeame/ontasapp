# Set the path
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from datetime import datetime

from flask.ext.script import Manager, Server
from app import app

manager = Manager(app)

from app import user_datastore
from flask.ext.security.utils import encrypt_password

from app.mod_tracker.models import *

# Turn on debugger by default and reloader
manager.add_command("runserver", Server(
    use_debugger = True,
    use_reloader = True,
    host = '0.0.0.0')
)


@manager.command
def create_adminRole():
	"Create Admin Role"
	user_datastore.create_role(name="Admin", description="The site administrator")

@manager.command
def create_admin():
    "Creates the admin user."
    user = user_datastore.create_user(email='miguel@codea.me', password=encrypt_password('qwerty'), confirmed_at=datetime.now(), active=True)
    role = user_datastore.find_role('Admin') or user_datastore.create_role(name="Admin", description="The site administrator")
    print 'user created...'
    if user_datastore.add_role_to_user(user, role):
    	user = user_datastore.find_user(email="miguel@codea.me")
    	print 'user updated...'


    print 'User miguel@codea.me was created:\n%s\n%s'%(user, role)


@manager.command
def create_defaultDevice():
    "Creates de default deviceModel and device, and adds to the default user (miguel@codea.me) for testing purposes."
    user = user_datastore.find_user(email='miguel@codea.me')
    if user is not None:
        print 'User miguel@codea.me found, creating device...'
        try:
            devModel = DeviceModel.objects.get(name='vt310')
            print 'Device Model already created!'
        except DeviceModel.DoesNotExist:
            devModel = DeviceModel(name='vt310')
            devModel.save()
            print 'Creating default DeviceModel:\n%s'%devModel
        #now the device
        try:
            device = Device.objects.get(serial='45221520187')
            print 'Device already created!'
        except Device.DoesNotExist:
            device = Device(serial='45221520187', name='GPS 1', model=devModel, owner=user)
            device.save()
            print 'Creatng device:\n%s'%device

@manager.command
def drop_defaultDevice():
    "Drops the default device"
    d = Device.objects(name='GPS 1')
    d.delete()

@manager.command
def add_testDevices():
    "Add test device"
    try:
        dm = devModel = DeviceModel.objects.get(name='vt310')
    except Device.DoesNotExist:
        dm = DeviceModel(name='vt310')
        dm.save()
    du = user_datastore.create_user(email='test@codea.me', password=encrypt_password('test'), confirmed_at=datetime.now(), active=True)
    role = user_datastore.find_role('Tester') or user_datastore.create_role(name="Tester", description="Tester")
    if user_datastore.add_role_to_user(du, role):
        print 'Role <Test> assigned to user <test@codea.me>'

    d = Device(name='Testing Device', serial='10000000001', model=dm, owner=du)
    d.save()

@manager.command
def test_device():
    dev = Device() #just testing valid code :)

if __name__ == "__main__":
    manager.run()