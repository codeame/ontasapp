
from secrets import *


DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))


# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED     = True

#Flask-Security
SECURITY_CONFIRMABLE = True
SECURITY_TRACKABLE = True
SECURITY_REGISTERABLE = False #True 
SECURITY_CHANGEABLE = True
SECURITY_RECOVERABLE = True
SECURITY_SEND_REGISTER_EMAIL = True
SECURITY_LOGIN_WITHOUT_CONFIRMATION = False #True
SECURITY_PASSWORD_HASH = 'bcrypt'
#SECURITY_URL_PREFIX = '/acc'
SECURITY_TOKEN_AUTHENTICATION_KEY = 'auth_token'
SECURITY_TOKEN_AUTHENTICATION_HEADER = 'Authentication-Token'
SECURITY_REGISTER_URL = '/registrar'
SECURITY_CHANGE_URL = '/cambiar_clave'
SECURITY_EMAIL_SUBJECT_REGISTER = 'Bienvenido'
SECURITY_EMAIL_SUBJECT_CONFIRM = 'Por favor confirme su correo'
SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = 'Su password ha sido reseteado.'
SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE = 'Su password ha sido cambiado.'
SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = 'Instrucciones para resetear su password.'

#Los siguientes tres no los encontre en la documentacion, pero en: 
#http://stackoverflow.com/questions/23136081/how-to-override-flask-security-default-messages
SECURITY_MSG_INVALID_PASSWORD = ("Usuario o password incorrecto", "error")
SECURITY_MSG_PASSWORD_NOT_PROVIDED = ("Debe proporcionar el password", "error")
SECURITY_MSG_USER_DOES_NOT_EXIST = ("Usuario no existe", "error")
SECURITY_MSG_EMAIL_ALREADY_ASOCIATED = ("Email ya esta registrado", "error")
SECURITY_MSG_EMAIL_NOT_PROVIDED = ("Debe proporcionar el correo electronico", "error")
SECURITY_MSG_PASSWORD_INVALID_LENGHT = ("La longitud del password es invalida", "error")
SECURITY_MSG_RETYPE_PASSWORD_MISMATCH = ("Los passwords no concuerdan", "error")
SECURITY_MSG_ALREADY_CONFIRMED = ("Correo electronico ya esta confirmado", "error")
SECUIRTY_MSG_CONFIRMATION_REQUIRED = ("Se requiere confirmacion", "error")
SECURITY_MSG_DISABLED_ACCOUNT = ("Cuenta deshabilitada", "error")
SECURITY_MSG_PASSWORD_IS_THE_SAME = ("El password es el mismo", "error")
SECURITY_MSG_INVALID_EMAIL_ADDRESS = ("Correo electronico invalido", "error")


DEBUG_TB_INTERCEPT_REDIRECTS = False
DEBUG_TB_PANELS = [
        'flask_debugtoolbar.panels.versions.VersionDebugPanel',
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.template.TemplateDebugPanel',
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
        #'flask.ext.mongoengine.panels.MongoDebugPanel' 
        # disabled the Config Vars panel for security reasons (db password app secret).
        # SEE: https://github.com/phleet/flask_debugtoolbar_lineprofilerpanel
        ]

