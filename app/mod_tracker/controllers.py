# Import flask dependencies
from flask import Blueprint, request, render_template, flash, g, \
    session, redirect, flash, redirect, abort, url_for

#from flask.ext.login import login_required, current_user
from flask_security.core import current_user
from flask_security.decorators import login_required

#geolocation stuff
import geojson

# Import the database object from the main app module
from app import db

# Import module forms
#from app.mod_tracker.forms import *

# Import module models
from app.mod_tracker.models import *

# Define the blueprint: 'tracker', set its url prefix: app.url/
mod_tracker = Blueprint('tracker', __name__, url_prefix='')

#routes

@mod_tracker.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    devices = Device.objects.filter(owner = current_user.id)
    dev_list = []
    for d in devices:
        #this is not optimum, but i cannot find another solution.. and users will not have thousand devices..
        dev_list.append(d.serial)
    pos = Position.objects(device_id__in = dev_list)
    alarms = Alarm.objects(device_id__in = dev_list)
    
    #FST-30-84
    
    return render_template('tracker/dashboard.html',  devices=devices, positions=pos, alarms=alarms)
