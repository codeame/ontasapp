# Import Form
from flask.ext.wtf import Form 

# Import Form elements such as TextField and BooleanField (optional)
from wtforms import TextField, PasswordField # BooleanField

# Import Form validators
from wtforms.validators import Required, Email, EqualTo


# Define the login form (WTForms)

class LoginForm(Form):
    email    = TextField('Correo electr^oacute;nico', [Email(),
                Required(message='Olvido proporcionar su correo.')])
    password = PasswordField('Password', [
                Required(message='Debe proporcionar su password.')])
