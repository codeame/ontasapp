# Import flask dependencies
from flask import Blueprint, request, render_template, flash, g, \
    session, redirect, url_for, flash, redirect, abort

# Import password / encryption helper tools
from werkzeug import check_password_hash, generate_password_hash

# Import the database object from the main app module
from app import db

# Import module forms
from app.mod_auth.forms import LoginForm

# Import module models (i.e. User)
from app.mod_auth.models import User
from flask.ext.login import login_user

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_auth = Blueprint('auth', __name__, url_prefix='/a')
