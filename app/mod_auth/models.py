from mongoengine import *
from flask.ext.security import UserMixin, RoleMixin



# User and Role for flask-Security
class Role(Document, RoleMixin):
    name = StringField(max_length=80, unique=True)
    description = StringField(max_length=255)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return str(self.name)


class User(Document, UserMixin):
    email = StringField(max_length=255, unique=True)
    password = StringField(max_length=255)
    active = BooleanField(default=True)
    confirmed_at = DateTimeField()
    roles = ListField(ReferenceField(Role), default=[])
    #login tracking
    last_login_at = DateTimeField()
    current_login_at = DateTimeField()
    current_login_ip = StringField(max_length=20)
    last_login_ip = StringField(max_length=20)
    login_count = IntField()
    #Remember auth
    remember_token = StringField(max_length=255)
    authentication_token = StringField(max_length=255)

    @property
    def is_admin(self):
        for rol in self.roles:
            if (rol.name == "Admin"): #this is hardcoded: the role must be named "Admin"!
                return True
        return False

    def __unicode__(self):
        return self.email

    def __repr__(self):
        return str(self.email)
