# Import flask and template operators
from flask import Flask, render_template

from flask.ext.mongoengine import MongoEngine
from flask.ext.security import Security, MongoEngineUserDatastore, login_required
from flask_mail import Mail
from flask.ext.moment import Moment
from flask.ext import admin, login

from flask_debugtoolbar import DebugToolbarExtension

from app.mod_auth.models import User, Role


# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')


# Define the database object which is imported
# by modules and controllers
db = MongoEngine(app)

# HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


# Import a module / component using its blueprint handler variable (mod_auth)
#from ontas.app.mod_auth.controllers import mod_auth as auth_module
from app.mod_auth.controllers import mod_auth as auth_module
from app.mod_tracker.controllers import mod_tracker as tracker_module

# Register blueprint(s)
app.register_blueprint(auth_module) # ,subdomain)
app.register_blueprint(tracker_module)


user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)

mail = Mail(app)
moment = Moment(app)
toolbar = DebugToolbarExtension(app)