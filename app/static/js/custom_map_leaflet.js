$(document).ready(function() {
    $( window ).load(function() {
        initialize();
    });

    function initialize() {

        var map = L.map('map-canvas').setView([19.244546523606317, -103.72501373291016], 13);

        var ggl = new L.Google('ROADMAP');
        map.addLayer(ggl);

        map.addControl( new L.Control.Compass() );

        var sw = L.latLng(18.535580, -103.545013);
        var ne = L.latLng(19.597287, -104.665618);
        var bounds = L.latLngBounds(sw, ne);
        map.fitBounds(bounds);
        map.setMaxBounds(bounds);

        

        function reverseGeoCode(lat,lng, callback) {
            // Uses http://nominatim.openstreetmap.org/reverse?format=json&lat=19.244546523606317&lon=-103.72501373291016
            // http://wiki.openstreetmap.org/wiki/Nominatim
            // SEE USAGE POLICIES: http://wiki.openstreetmap.org/wiki/Nominatim_usage_policy
            // CONSIDER USE AN INTERMEDIATE DATABASE AT THE SERVER TO AVOID MANY QUERIES TO OSM.
            // MapQuest offers this services with no limits: http://open.mapquestapi.com/nominatim/
            var firstPart = 'http://nominatim.openstreetmap.org/reverse?format=json';
            var url = firstPart + '&lat='+lat+ '&lon='+lng;
            $.ajax({
              dataType: "json",
              url: url,
              success: callback
            });
            console.log("Going to call getNearPoints() with Lat:"+lat+" Lon:"+lng);
            getNearPoints(lat,lng, 500);
        }
         
        function displayAddress(data) {
            $('#alrededoresTitle').html('<h5>Alrededores. <small>Est\u00e1 alrededor de '+data['display_name']+'</small> &nbsp;&nbsp;&nbsp;'+
                '<div class="btn-group btn-right-abs"> <button class="btn btn-default btn-xs dropdown-toggle btn-opciones" type="button" data-toggle="dropdown"> <span class="fa fa-sliders"></span> <span class="caret"></span></button>'+
                '<ul class="dropdown-menu">'+
                ' <li id="radioLeyend" role="presentation" class="dropdown-header">Radio de distancia: 500 metros.</li>'+
                '<div class="well"><input type="text" class="span2" id="sliderRadio" value="500" data-slider-min="100" data-slider-max="5000" data-slider-step="100" data-slider-orientation="horizontal-menu" data-slider-value="500" data-slider-selection="after"></div>'+
                '</ul></div></h5>');
            console.log(data);
            $('#sliderRadio').slider().on('slideStop', function(ev){
                $('#radioLeyend').text('Radio de distancia: '+ev.value +' metros.');
                //again, call getNear
                r = ev.value;
                //WARNING: Note that the lon/lat values comming from the reversegeocode is TRUNCATED to a precission of 6 digits, ths make it DIFFERENT (moved compared to the original latlon)
                //lat = data['lat'];
                //lon = data['lon'];
                //so better to use a global varible to store the latLng.
                console.log("@displayAddress INSIDE EVENT LISTENER FOR SLIDER");
                lat = locationPoint.lat;
                lon = locationPoint.lng;
                console.log("@displayAddres::sliderEvent| going to call getNearPoints with Lat:"+lat+" LonL"+lon+" and R:"+r+" See data above.");
                getNearPoints(lat,lon, r);
            });//on slider slideStop
        }

        

        function onEachFeature(feature, layer) {
            //constuct the html for the popup
            if (feature.properties) {
                var content = 
                '<div class="media m-wider">'+
                  '<a class="pull-left" href="#">'+
                    feature.properties.logo +
                  '</a>'+
                  '<div class="media-body">'+
                    '<h4 class="media-heading bg-myinfo wider">'+feature.properties.nombre+' '+feature.properties.featured+'</h4>'+
                    '<h5 class="minimum-margins"><small>'+feature.properties.direccion+'<br><span class="glyphicon glyphicon-earphone"></span>&nbsp;'+feature.properties.telefono +'</small></h5>'+ //municipio is intented to be used to filter queries at db level. right?
                    feature.properties.tag_imgs+'<br>'+
                  '</div>'+
                '</div>';
                layer.bindPopup(content);
            }
        } //onEachFeature()

        function onEachMarker(feature, latLng) {
            switch (feature.properties.tags[0].toLowerCase() ) {
                case 'abogados':        return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix: 'fa', icon: 'legal', markerColor: 'cadetblue', iconColor: 'white'})});
                case 'hoteles':        return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix: 'fa', icon: 'building', markerColor: 'blue', iconColor: 'white'})});
                case 'restaurantes':   return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({icon: 'cutlery', markerColor: 'orange', iconColor: 'white'})});
                case 'hospitales':     return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({icon: 'plus', markerColor: 'red', iconColor: 'white'})});
                case 'tiendas de conveniencia': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({icon: 'shopping-cart', markerColor: 'green', iconColor: 'white'})});
                case 'tiendas especializadas': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix:'fa', icon: 'rocket', markerColor: 'darkred', iconColor: 'white'})});
                case 'construccion': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({icon: 'tower', markerColor: 'darkred', iconColor: 'white'})});
                case 'ferreterias': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix:'fa', icon: 'wrench', markerColor: 'darkpurple', iconColor: 'white'})});
                case 'de todo para el hogar': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({icon: 'home', markerColor: 'cadetblue', iconColor: 'white'})});
                case 'medicos': return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix:'fa', icon: 'user-md', markerColor: 'purple', iconColor: 'white'})});
                case 'universidades':  return L.marker(latLng, {riseOnHover:true, icon: L.AwesomeMarkers.icon({prefix:'fa', icon: 'university', markerColor: 'blue', iconColor: 'yellow'})});
                // WARNING: Do not forget to exapnd this list when a new CatTags is added, because if not it will cause ERROR and NO MARKER will be SHOWN.
            }
        }
        
        //adding an empty geojson layer, to later add my json data from my api.
        var myLayer = L.geoJson('',{ onEachFeature: onEachFeature, pointToLayer: onEachMarker}).addTo(map);

        // Get POIs from my API.

        function AddMarkers(data) {
            //Receives a collection of features. Could be 1 or more points; or an empty array.
            console.log(data)
            myLayer.addData(data);
        }

        function showMarkers() {
            // get map bounds (at the viewport)
            var bounds = map.getBounds(); // Returns a object that has the NorthEast and SouthWest LatLng points of the bounds

            // Call you server with ajax passing it the bounds
            // In the ajax callback delete the current markers and add new markers
            // This code is not parsed by jinja because it is not INSIDE the .html template {{ url_for('AjaxPointsWithinBox') }}
            $.getJSON($SCRIPT_ROOT + '/getPointsWithinBox', {
                ne: encodeURI(bounds.getNorthEast().toString()),
                sw: encodeURI(bounds.getSouthWest().toString())
            }, function(data) {
            //TODO: REMOVE OLD MARKERS to save memory :)  ?
            if(data.errors) {
                  console.log("ERROR: data error:"+data.errors);
                } else {
                    console.log(data);
                    AddMarkers(data);
                }
            });
        } //showMarkers()

        //trigger the data load. Not using the maps on idle listener.
        // MCH :: showMarkers();


    }; //end of initialize()
});
