# -*- coding: utf-8 -*-


# ontas application
# (C) 2015 by
# Miguel Chavez Gamboa
# http://www.codea.me/ontas


from app import app



app.run(host='0.0.0.0', port=8080, debug=True)
